import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Input, Label } from "reactstrap";
import Person from "../../assets/images/Employee-512.webp";

const Form = () => {
    return (
        <>
            <div className="row text-center">
                <h2>HỒ SƠ NHÂN VIÊN</h2>
            </div>
            <div className="row mt-4">
                <div className="col-sm-9">
                    <div className="row mt-4">
                        <div className="col-sm-4"><Label>Họ và tên</Label></div>
                        <div className="col-sm-8"><Input type="text"/></div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-sm-4"><Label>Ngày sinh</Label></div>
                        <div className="col-sm-8"><Input type="date"/></div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-sm-4"><Label>Số điện thoại</Label></div>
                        <div className="col-sm-8"><Input type="number"/></div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-sm-4"><Label>Giới tính</Label></div>
                        <div className="col-sm-8">
                            <Input type="select" name="select">
                                <option>Nam</option>
                                <option>Nữ</option>
                                <option>Khác</option>
                            </Input>
                        </div>
                    </div>
                </div>
                <div className="col-sm-3">
                    <img src={Person} width="100%"/>
                </div>
            </div>
            <div className="row mt-4">
                <div className="col-sm-3">
                    <Label>Công việc</Label>
                </div>
                <div className="col-sm-9">
                    <Input type="textarea"></Input>
                </div>
            </div>
            <div className="row mt-4">
                <div className="col-sm-6"></div>
                <div className="col-sm-6">
                    <div className="row">
                        <div className="col-sm-6"><Button color="primary" className="w-100">Chi tiết</Button></div>
                        <div className="col-sm-6"><Button color="primary" className="w-100">Kiểm tra</Button></div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Form