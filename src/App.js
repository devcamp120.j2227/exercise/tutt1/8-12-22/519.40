import { Container } from 'reactstrap';
import Form from './components/form/form';
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <Container className='mt-5'>
      <Form/>
    </Container>
  );
}

export default App;
